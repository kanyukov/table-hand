import {Group} from "konva/types/Group";
import Item from "@/api/Item";

export type HandItem = {
    face_src: string,
    backface_src: string,
    id: string,
    order?: number,
    width: number,
    height: number,
    x: number,
    zIndex: number
}

export type TableItem = HandItem & {
    y: number
}

export type CardSetup = {
    face_src: string,
    backface_src: string,
    id: string
}

export type ConnectorStageOptions = {
    item: Item,
    newX: number,
    newY: number,
}

export type AnimateItemOnConnectorStageOptions = {
    item: Item,
    newX: number,
    newY: number,
    newX1: number,
    newY1: number,

}

export type ConnectorStageAddItemOptions = {
    item: Item,
    oldItem: Item
}

export type RenderConfig = {
    handContainer: HTMLElement,
    handImages: Array<CardSetup>,
    tableContainer: HTMLElement,
    tableImages: Array<CardSetup>,
    zoomContainer: HTMLElement,
    connectorContainer: HTMLElement,
    tableColumns: number,
    tableLayoutPreset: null|string
}

export type DeckOptions = {
    x: number,
    y: number,
    width: number,
    height: number
}