/**
 * Апи для работы с рукой
 */
import {ConnectorStageOptions, DeckOptions, HandItem, TableItem} from "@/types/types";
import {Group} from "konva/types/Group";
import EventApi from "@/api/EventsApi";
import RenderManager from "@/api/render/RenderManager";
import Item from "@/api/Item";

export default class Deck {
    private deck: Group|null = null;
    private container: HTMLElement|null = null;
    private deckItems: Array<Item> = [];

    /**
     * Constructor
     */
    constructor(items: Array<Item>, container: HTMLElement, options: DeckOptions)
    {
        this.setDeckItems(items);
        this.setContainer(container);
        this.createDeck(items, options);
        this.bindEvents();
    }

    /**
     * Установить контейнер
     * @param container
     */
    public setContainer(container: HTMLElement): void
    {
        this.container = container;
    }

    /**
     * Установить итемы колоды
     * @param items
     */
    public setDeckItems(items: Array<Item>): void
    {
        this.deckItems = items;
    }

    /**
     * Получить итемы колоды
     */
    public getDeckItems(): Array<Item>
    {
        return this.deckItems;
    }

    /**
     * Создать итем руки
     */
    public createDeck(items: Array<Item>, options: DeckOptions): void
    {
        this.deck = RenderManager.renderTableDeck(items, options);
    }

    /**
     * Получить итем руки
     */
    public getItem(): Group|null
    {
        return this.deck;
    }

    /**
     * Перевернуть итем
     */
    public flipItem(): void
    {
        if (!this.deck) {
            return;
        }
        RenderManager.flipItem(this.deck);
    }

    /**
     * Удалить итем руки
     */
    public deleteItem(): void
    {
        if (!this.deck) {
            return;
        }
        RenderManager.deleteItem(this.deck);
        //TODO: отсылать на серв ID карты для удаления
    }

    /**
     * Получить имя итема
     */
    public getItemName(): string
    {
        if (!this.deck) {
            return '';
        }

        return RenderManager.getItemName(this.deck);
    }

    /**
     * Получить ID итема
     */
    public getItemID(): string
    {
        if (!this.deck) {
            return '';
        }

        return RenderManager.getItemID(this.deck);
    }

    /**
     * Получить ширину итема
     */
    public getItemWidth(): number
    {
        if (!this.deck) {
            return 0;
        }

        return RenderManager.getItemWidth(this.deck);
    }

    /**
     * Получить ширину итема
     */
    public getItemHeight(): number
    {
        if (!this.deck) {
            return 0;
        }

        return RenderManager.getItemHeight(this.deck);
    }

    /**
     * Установить позицию х итема
     */
    public setItemPosition(position: {[key: string]: number}): void
    {
        if (!this.deck) {
            return;
        }

        RenderManager.setTableItemPosition(this.deck, position);
    }

    /**
     * Получить Y итема
     */
    public getItemYPosition(): number
    {
        if (!this.deck) {
            return 0;
        }

        return RenderManager.getItemYPosition(this.deck);
    }

    private bindEvents(): void
    {

    }
}