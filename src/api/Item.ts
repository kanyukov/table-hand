/**
 * Апи для работы с рукой
 */
import {ConnectorStageOptions, HandItem, TableItem} from "@/types/types";
import {Group} from "konva/types/Group";
import EventApi from "@/api/EventsApi";
import RenderManager from "@/api/render/RenderManager";

export default class Item {
    private cardItem: Group|null = null;
    private container: HTMLElement|null = null;
    private itemType: string = '';
    private itemConfig: HandItem|TableItem|null = null;

    /**
     * Constructor
     */
    constructor(item: HandItem|TableItem, container: HTMLElement, type: string)
    {
        this.setItemConfig(item);
        this.setContainer(container);
        this.setType(type);
        this.createItem(item);
        this.bindEvents();
    }

    /**
     * Записать конфиг итема
     * @param config
     */
    public setItemConfig(config: HandItem|TableItem): void
    {
        this.itemConfig = config;
    }

    /**
     * Получить конфиг итема
     */
    public getItemConfig(): HandItem|TableItem|null
    {
        return this.itemConfig;
    }

    /**
     * Установить контейнер
     * @param container
     */
    public setContainer(container: HTMLElement): void
    {
        this.container = container;
    }

    /**
     * Установить тип
     * @param type
     */
    public setType(type: string): void
    {
        this.itemType = type;
    }

    /**
     * Создать итем руки
     * @param item
     */
    public createItem(item: HandItem|TableItem): void
    {
        if (this.itemType === 'hand') {
            this.cardItem = RenderManager.renderHandItem(item as HandItem);
        }
        if (this.itemType === 'table') {
            this.cardItem = RenderManager.renderTableItem(item as TableItem);
        }
    }

    /**
     * Получить итем руки
     */
    public getItem(): Group|null
    {
        return this.cardItem;
    }

    /**
     * Перевернуть итем
     */
    public flipItem(): void
    {
        if (!this.cardItem) {
            return;
        }
        RenderManager.flipItem(this.cardItem);
    }

    /**
     * Выбрать итем
     */
    public selectItem(): void
    {
        if (!this.cardItem) {
            return;
        }

        RenderManager.selectItem(this.cardItem);
    }

    /**
     * Выбрать итем
     */
    public deselectItem(): void
    {
        if (!this.cardItem) {
            return;
        }
        RenderManager.deselectItem(this.cardItem);
    }

    /**
     * Удалить итем руки
     */
    public deleteItem(): void
    {
        if (!this.cardItem) {
            return;
        }
        RenderManager.deleteItem(this.cardItem);
        //TODO: отсылать на серв ID карты для удаления
    }

    /**
     * Установить позицию х итема
     * @param x
     */
    public setXPosition(x: number): void
    {
        if (!this.cardItem) {
            return;
        }
        if (this.itemType === 'hand') {
            RenderManager.setHandItemXPosition(this.cardItem, x);
        }
    }

    /**
     * Установить позицию х итема
     */
    public setItemPosition(position: {[key: string]: number}): void
    {
        if (!this.cardItem) {
            return;
        }
        if (this.itemType === 'table') {
            RenderManager.setTableItemPosition(this.cardItem, position);
        }
    }

    /**
     * Получить Х итема
     */
    public getItemXPosition(): number
    {
        if (!this.cardItem) {
            return 0;
        }

        return RenderManager.getItemXPosition(this.cardItem);
    }

    /**
     * Получить Y итема
     */
    public getItemYPosition(): number
    {
        if (!this.cardItem) {
            return 0;
        }

        return RenderManager.getItemYPosition(this.cardItem);
    }

    /**
     * Получить имя итема
     */
    public getItemName(): string
    {
        if (!this.cardItem) {
            return '';
        }

        return RenderManager.getItemName(this.cardItem);
    }

    /**
     * Получить ID итема
     */
    public getItemID(): string
    {
        if (!this.cardItem) {
            return '';
        }

        return RenderManager.getItemID(this.cardItem);
    }

    /**
     * Получить ширину итема
     */
    public getItemWidth(): number
    {
        if (!this.cardItem) {
            return 0;
        }

        return RenderManager.getItemWidth(this.cardItem);
    }

    /**
     * Получить ширину итема
     */
    public getItemHeight(): number
    {
        if (!this.cardItem) {
            return 0;
        }

        return RenderManager.getItemHeight(this.cardItem);
    }

    /**
     * Установить ширину итема
     */
    public setItemWidth(width: number): void
    {
        if (!this.cardItem) {
            return;
        }

        RenderManager.setItemWidth(this.cardItem, width);
    }

    /**
     * Установить ширину итема
     */
    public setItemHeight(height: number): void
    {
        if (!this.cardItem) {
            return;
        }

        RenderManager.setItemHeight(this.cardItem, height);
    }

    /**
     * Установить z-index итема
     */
    public setZIndex(index: number): void
    {
        if (!this.cardItem) {
            return;
        }

        RenderManager.setItemZIndex(this.cardItem, index);
    }

    /**
     * Получить урл лицевой стороны карты
     */
    public getItemFaceUrl(): string
    {
        if (!this.cardItem) {
            return '';
        }

        return RenderManager.getItemFaceUrl(this.cardItem);
    }

    /**
     * Получить урл рубашки карты
     */
    public getItemBackfaceUrl(): string
    {
        if (!this.cardItem) {
            return '';
        }

        return RenderManager.getItemBackfaceUrl(this.cardItem);
    }

    private getClonedToConnectorItemOptions(): ConnectorStageOptions|null
    {
        if (!this.container) {
            return null;
        }

        let x = 0;
        let y = 0;
        let options = {};
        const parentX = this.container.getBoundingClientRect().x;
        const parentY = this.container.getBoundingClientRect().y;
        if (this.cardItem) {
            x = RenderManager.getItemXPosition(this.cardItem) + parentX;
            y = RenderManager.getItemYPosition(this.cardItem) + parentY;
            options = {
                item: this,
                newX: x,
                newY: y
            }
        }

        return options as ConnectorStageOptions;
    }

    dragstartEventInit(): void
    {
        if (!this.cardItem) {
            return;
        }

        this.cardItem.on('dragstart', () => {
            const options = this.getClonedToConnectorItemOptions();
            if (options) {
                RenderManager.createConnectorStage(options);
            }
        });
    }

    /**
     * Клонировать итем
     */
    public cloneItem(): Item
    {
        return new Item({
            face_src: this.getItemFaceUrl(),
            backface_src: this.getItemBackfaceUrl(),
            id: this.getItemID(),
            width: this.getItemWidth(),
            height: this.getItemHeight(),
            x: this.getItemXPosition(),
            zIndex: 1
        }, this.container as HTMLElement, this.itemType as string)
    }

    /**
     * Анимировать перемещаение из руки на стол
     * @private
     */
    public animateMovingFromHandToTable(): void
    {
        const options = Object.assign({newX1: 250, newY1: 250}, this.getClonedToConnectorItemOptions());
        RenderManager.animateItemMovingOnConnectorStage(options);
    }

    private bindEvents(): void
    {
        if (this.itemType === 'hand') {
            this.dragstartEventInit();
        }

        this.cardItem?.addEventListener(EventApi.FLIP_SELECTED_ITEM, this.flipItem);
        this.cardItem?.addEventListener(EventApi.DELETE_SELECTED_ITEM, this.deleteItem);


    }
}