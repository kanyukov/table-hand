import {RenderConfig} from "@/types/types";
import Hand from "@/api/Hand";
import Table from "@/api/Table";
import EventApi from "@/api/EventsApi";
import Item from "@/api/Item";
import Deck from "@/api/Deck";

export default class TableHand {
    private hand: Hand|null = null;
    private table: Table|null = null;
    /**
     * Constructor
     * @param {Object} config
     */
    constructor(config: RenderConfig)
    {
        this.createHand(config);
        this.createTable(config);
        this.bindEvents();
    }

    /**
     * Создать руку
     * @private
     */
    private createHand(config: RenderConfig): void
    {
        this.hand = new Hand(config);
    }

    /**
     * Создать руку
     * @private
     */
    private createTable(config: RenderConfig): void
    {
        this.table = new Table(config);
    }

    /**
     * Удалить выбранные эл-ты руки
     */
    public deleteSelectedHandItems(): void
    {
        this.hand?.deleteSelectedItems(null);
    }

    /**
     * Удалить hovered эл-ты руки
     */
    public deleteHoveredHandItems(): void
    {
        this.hand?.deleteHoveredItems();
    }

    /**
     * Перевернуть выбранные эл-ты руки
     */
    public flipSelectedHandItems(): void
    {
        this.hand?.flipSelectedItems();
    }

    /**
     * Увеличить выбранные эл-ты руки
     */
    public zoomSelectedHandItems(): void
    {
        this.hand?.zoomSelectedItems();
    }

    /**
     * Добавить эл-т в руку
     * @param imgUrl
     */
    public addHandItem(imgUrl: string): void
    {
        this.hand?.addItem(imgUrl);
    }

    /**
     * Обновить позиции эл-тов в руке
     */
    public refreshHandItemsPositions(): void
    {
        if (!this.hand) {
            return;
        }
        this.hand.refreshCardsXPosition();
    }

    /**
     * Удалить выбранные эл-ты руки
     */
    public deleteSelectedTableItems(): void
    {
        this.table?.deleteSelectedItems(null);
    }

    /**
     * Перевернуть выбранные эл-ты руки
     */
    public flipSelectedTableItems(): void
    {
        this.table?.flipSelectedItems();
    }

    /**
     * Увеличить выбранные эл-ты руки
     */
    public zoomSelectedTableItems(): void
    {
        this.table?.zoomSelectedItems();
    }

    /**
     * Добавить итем руки на стол
     */
    private addTableItemFromHand(data: any): void
    {
        if (!data || !this.table) {
            return;
        }
        let items;
        let center = false;

        if (data instanceof CustomEvent) {
            items = data.detail;
        } else {
            items = data;
            center = true
        }

        this.table?.addItemsFromHand(items, center);
    }

    /**
     * Получить выбранные итемы руки
     * @private
     */
    private getSelectedHandItems(): Array<Item>|null
    {
        if (!this.hand) {
            return null;
        }
        return this.hand.getSelectedItems();
    }

    /**
     * Получить выбранные итемы руки
     * @private
     */
    private getHoveredHandItems(): Array<Item>|null
    {
        if (!this.hand) {
            return null;
        }
        return this.hand.getHoveredItems();
    }

    /**
     * Добавить эл-т на стол с компьютера
     * @param imgUrls
     * @param cols
     * @param isDeck
     */
    public addTableItems(imgUrls: Array<string>, cols: number, isDeck: boolean): void
    {
        this.table?.addItems(imgUrls, cols, isDeck);
    }

    /**
     * Переместить эл-т из руки на стол
     * @param e
     * @private
     */
    private addTableItemsOnKeyup(e: any): void
    {
        const keyTCode = 'KeyT';
        if (!EventApi.getPreventKeyEvents() && e.code === keyTCode) {
            EventApi.setPreventKeyEvents(true);
            let itemsToMove: Array<Item> = [];
            const selectedItems = this.getSelectedHandItems();
            const hoveredItems = this.getHoveredHandItems();

            if (selectedItems) {
                itemsToMove = [...itemsToMove, ...selectedItems];
            }

            if (hoveredItems) {
                itemsToMove = [...itemsToMove, ...hoveredItems];
            }

            if (itemsToMove.length) {
                this.addTableItemFromHand(itemsToMove);
                this.deleteSelectedHandItems();
                this.deleteHoveredHandItems();
                // TODO анимация
                // selectedItems.forEach((item) => {
                //     EventApi.triggerEvent(EventApi.HAND_TO_TABLE_ANIMATE, item);
                // })
            }
        }
    }

    private zoomItemsOnKeyup(e: any): void
    {
        const keySpaceCode = 'Space';
        if (!EventApi.getPreventKeyEvents() && e.code === keySpaceCode) {
            EventApi.setPreventKeyEvents(true);
            this.zoomSelectedHandItems();
            // TODO для стола
            // this.zoomSelectedTableItems();
        }
    }

    private bindKeyUpEvents(e: any): void
    {
        this.addTableItemsOnKeyup(e);
        this.zoomItemsOnKeyup(e);
    }

    /**
     * Создать колоду из карт на столе
     */
    public createDeckFromSelectedTableCards(): void
    {
        this.table?.createDeckFromSelectedCards();
    }

    /**
     * Получить выбранные колоды
     */
    public getSelectedDecks(): Array<Deck>
    {
        if (!this.table) {
            return [];
        }
        return this.table.getSelectedDecks();
    }

    /**
     * Перемешать выбранную колоду
     */
    public shuffleDeck(): void
    {
        this.table?.shuffleDeck();
    }

    /**
     * Снять выделение выбранных эл-тов
     * @private
     */
    private deselectItems(e: Event): void
    {
        if ((e.target as HTMLElement).tagName !== 'CANVAS') {
            EventApi.triggerEvent(EventApi.DESELECT_HAND_ITEMS);
            EventApi.triggerEvent(EventApi.DESELECT_TABLE_ITEMS);
        }
    }

    private bindEvents(): void
    {
        EventApi.addEventListener(EventApi.REFRESH_HAND_ITEMS_ALL_POSITIONS, this.refreshHandItemsPositions);
        EventApi.addEventListener(EventApi.ADD_TABLE_ITEM, this.addTableItemFromHand.bind(this));
        EventApi.addEventListener('keyup', this.bindKeyUpEvents.bind(this));
        EventApi.addEventListener('click', this.deselectItems.bind(this));
    }

    public unbindEvents(): void
    {
        EventApi.removeEventListener(EventApi.REFRESH_HAND_ITEMS_ALL_POSITIONS, this.refreshHandItemsPositions);
        EventApi.removeEventListener(EventApi.ADD_TABLE_ITEM, this.addTableItemFromHand.bind(this));
        EventApi.removeEventListener('keyup', this.bindKeyUpEvents.bind(this));
        EventApi.removeEventListener('click', this.deselectItems.bind(this));
    }
}