/**
 * Апи для работы с рукой
 */
import {CardSetup, DeckOptions, HandItem, RenderConfig, TableItem} from "@/types/types";
import Item from "@/api/Item";
import {Layer} from "konva/types/Layer";
import EventApi from "@/api/EventsApi";
import RenderManager from "@/api/render/RenderManager";
import RenderHandSceneManager from "@/api/render/RenderHandSceneManager";
import RenderHandManager from "@/api/render/RenderHandManager";
import RenderTableManager from "@/api/render/RenderTableManager";
import RenderTableSceneManager from "@/api/render/RenderTableSceneManager";
import {Group} from "konva/types/Group";
import Deck from "@/api/Deck";

export default class Table {
    private table: Layer|null = null;
    private readonly container: null|HTMLElement = null;
    private tableItems: Array<Item|Deck> = [];
    private readonly itemsLength: number = 0;
    private readonly rows: number = 1;
    private readonly cols: number = 1;
    private readonly containerPadding: number = 20;
    private readonly itemGap: number = 25;
    private readonly tableName: string = 'table';

    /**
     * Constructor
     * @param {Object} config
     */
    constructor(config: RenderConfig)
    {
        this.container = config.tableContainer;
        this.cols = config.tableColumns;
        this.itemsLength = config.tableImages.length;
        this.rows = Math.ceil(this.itemsLength / this.cols);
        this.createTable(config);
        this.buildItemsConfigs(config.tableImages, this.cols, this.rows).then(result => {
            this.createItems(result);
            setTimeout(() => {
                RenderTableSceneManager.zoomTableCenter(this.getZoomValue());
            }, 300);
        });
        this.bindEvents();
    }

    /**
     * Получить размеры эл-та
     * @param image
     * @param cols
     * @private
     */
    private getItemSize(image: HTMLImageElement, cols: number): {[key: string]: number}
    {
        const size = {
            width: 0,
            height: 0,
            maxWidth: 0,
            maxHeight: 0
        }

        if (!this.container) {
            return size;
        }

        size.maxWidth = Math.floor((this.container.offsetWidth - 2 * this.containerPadding - (cols - 1) * this.itemGap) / cols);
        const naturalWidth: number = Math.floor(image.naturalWidth);
        const naturalHeight: number = Math.floor(image.naturalHeight);
        let ratio;

        ratio = size.maxWidth / naturalWidth;

        size.width = Math.floor(naturalWidth * ratio);
        size.height = Math.floor(naturalHeight * ratio);
        size.maxHeight = size.height;

        return size;
    }

    /**
     * Получить позицию эл-та
     * @param width
     * @param height
     * @param index
     * @private
     */
    private getItemPosition(width: number|null, height: number|null, index: number, cols: number, rows: number): {[key: string]: number}|null
    {
        if (!width || !height) {
            return null;
        }
        const position = {
            x: 0,
            y: 0
        }

        if (!this.container) {
            return position;
        }
        if (cols === 1 && rows === 1) {
            position.x = Math.floor(this.container.offsetWidth / 2 - width /2);
            position.y = Math.floor(this.container.offsetHeight / 2 - height /2);
        } else {
            const positionRow = (index % cols);
            const positionCol = (index - (index % cols)) / cols;
            position.x = Math.floor(this.containerPadding + this.itemGap * positionRow + width * positionRow);
            position.y = Math.floor(this.containerPadding + this.itemGap * positionCol + height * positionCol);
        }

        return position;
    }

    /**
     * Создать слой стола
     */
    private createTable(config: RenderConfig): void
    {
        RenderManager.renderTable(config);
        this.table = RenderManager.getTableInstance();
    }

    /**
     * Создать изображение
     * @param src
     * @private
     */
    private createImage(src: string): Promise<HTMLImageElement>
    {
        const image = new Image();
        return new Promise((resolve, reject) => {
            image.onload = () => {
                resolve(image);
            };
            image.onerror = reject;
            image.src = src;
        });
    }

    /**
     * Созать конфиг для итемов стола
     * @param cardsSetup
     * @param cols
     * @param rows
     * @private
     */
    private buildItemsConfigs(cardsSetup: Array<CardSetup>, cols: number, rows: number): Promise<Array<TableItem>>
    {
        const config: Array<TableItem> = [];
        let zIndex = cardsSetup.length - 1;
        const promises: Array<Promise<HTMLImageElement | void>> = [];
        return new Promise((resolve, reject) => {
            cardsSetup.forEach((card, index) => {
                promises.push(
                    this.createImage(card.face_src).then(result => {
                        const size = this.getItemSize(result, cols);
                        const position = this.getItemPosition(size.maxWidth, size.maxHeight, index, cols, rows);
                        if (position) {
                            config.push({
                                face_src: card.face_src,
                                backface_src: card.backface_src,
                                id: card.id,
                                width: size.width,
                                height: size.height,
                                x: position.x,
                                y: position.y,
                                zIndex: zIndex
                            });
                        }
                    })
                );
            });

            Promise.all(promises).then(() => {
                resolve(config);
            })
        });
    }

    /**
     * Создать итемы стола
     */
    private createItems(items: Array<TableItem>, isDeck?: boolean): void
    {
        if (isDeck) {
            this.buildDeck(items, this.containerPadding * 2, this.containerPadding * 2);
        } else {
            items.forEach((item) => {
                this.tableItems.push(new Item(item, this.container as HTMLElement, this.tableName));
            });
        }
    }

    /**
     * Получить выбранные эл-ты
     * @private
     */
    public getSelectedItems(): Array<Item|Deck>
    {
        return this.tableItems.filter((item) => {
            return item.getItemName() === 'selected';
        });
    }

    /**
     * Получить эл-ты по ID
     * @private
     */
    private getItemsById(id: string): Array<Item|Deck>
    {
        return this.tableItems.filter((item) => {
            return item.getItemID() === id;
        });
    }

    /**
     * Удалить выбранные эл-ты из массива эл-тов руки
     * @param items
     * @private
     */
    private deleteItemsFromItemsArray(items: Array<Item|Deck>): void
    {
        this.tableItems.forEach((item, index) => {
            items.forEach((selectedItem) => {
                const itemID = item.getItemID();
                const selectedItemID = selectedItem.getItemID();

                if (itemID === selectedItemID) {
                    this.tableItems.splice(index, 1);
                }
            });
        });
    }

    /**
     * Удалить выбранные итемы руки
     */
    public deleteSelectedItems(itemId: string|null): void
    {
        let selectedItems = this.getSelectedItems();

        if (itemId) {
            selectedItems = this.getItemsById(itemId);
        }

        selectedItems.forEach((item) => {
            item.deleteItem();
        });
        this.deleteItemsFromItemsArray(selectedItems);
        this.refreshItemsPosition();
        RenderManager.updateStage(this.tableItems.length, this.tableName);
    }

    /**
     * Перевернуть выбранные итемы руки
     */
    public flipSelectedItems(): void
    {
        this.getSelectedItems().forEach((item) => {
            item.flipItem();
        });
    }

    /**
     * Увеличить выбранные итемы руки
     */
    public zoomSelectedItems(): void
    {
        let images: Array<string> = [];
        this.getSelectedItems().forEach((item) => {
            if (item instanceof Item) {
                images.push(item.getItemFaceUrl());
            }
        });

        EventApi.triggerEvent(EventApi.ZOOM_ITEMS, images);
    }

    /**
     * Обновить позиции эл-тов
     * @private
     */
    private refreshItemsPosition(): void
    {
        this.tableItems.forEach((item, index) => {
            const itemWidth = item.getItemWidth();
            const itemHeight = item.getItemHeight();
            const position = this.getItemPosition(itemWidth, itemHeight, index, this.cols, this.rows);
            if (position && itemWidth) {
                item.setItemPosition({
                    x: position.x + itemWidth / 2,
                    y: position.y
                });
            }
        });

        RenderTableManager.refreshTable();
    }

    /**
     * Добавить итемы из руки
     * @param items
     * @param center
     * @private
     */
    public addItemsFromHand(items: Array<Item>, center: boolean): void
    {
        const configs: Array<TableItem> = [];
        const promises: Array<Promise<HTMLImageElement | void>> = [];
        items.forEach((item) => {
            const config: TableItem = {
                face_src: item.getItemFaceUrl(),
                backface_src: item.getItemBackfaceUrl(),
                id: item.getItemID(),
                width: 0,
                height: 0,
                x: item.getItemXPosition(),
                y: item.getItemYPosition(),
                zIndex: 1
            };

            const faceUrl = item.getItemFaceUrl();

            promises.push(
                this.createImage(faceUrl).then(result => {
                    if (!this.container) {
                        return;
                    }

                    const size = this.getItemSize(result, this.cols);
                    config.width = size.width;
                    config.height = size.height;
                    if (center) {
                        config.x = this.container.offsetWidth / 2 - config.width / 2;
                        config.y = this.container.offsetHeight / 2 - config.height / 2;
                    } else {
                        config.x = item.getItemXPosition() - config.width / 2;
                    }
                    configs.push(config);
                })
            );
        });


        Promise.all(promises).then(() => {
            this.createItems(configs);
            RenderTableManager.refreshTable();
            EventApi.setPreventKeyEvents(false);
        });
    }

    /**
     * Добавить итемы в руку
     * @param imgUrls
     * @param cols
     * @param isDeck
     */
    public addItems(imgUrls: Array<string>, cols: number, isDeck: boolean): void
    {
        const items: Array<CardSetup> = [];
        let rows = 0;
        let currentCols = 0;
        if (this.tableItems.length) {
            rows = Math.ceil((imgUrls.length + this.tableItems.length) / this.cols);
            currentCols = this.cols;
        } else {
            rows = cols === 1 ? 1 : Math.ceil(imgUrls.length / cols);
            currentCols = cols;
        }

        imgUrls.forEach((url) => {
            items.push({
                face_src: url,
                backface_src: '/backface.png',
                id: `card_id_${Date.now()}`
            });
        });

        this.buildItemsConfigs(items, currentCols, rows).then(result => {
            this.createItems(result, isDeck);
            this.refreshItemsPosition();
            setTimeout(() => {
                RenderTableSceneManager.zoomTableCenter(this.getZoomValue());

            }, 300);
        });
    }

    /**
     * Создать колоду карт
     * @param items
     * @param x
     * @param y
     * @private
     */
    private buildDeck(items: Array<TableItem>, x: number, y: number): void
    {
        const widthValues = items.map((item) => {
            return item.width;
        });
        const heightValues = items.map((item) => {
            return item.height;
        });
        const options: DeckOptions = {
            x: x,
            y: y,
            width: Math.max.apply(null, widthValues),
            height: Math.max.apply(null, heightValues)
        };
        const itemsArray: Array<Item> = [];

        items.forEach((item) => {
            const group = new Item(item, this.container as HTMLElement, this.tableName);
            if (group) {
                itemsArray.push(group);
            }
        });

        if (this.container) {
            this.tableItems.push(new Deck(itemsArray, this.container, options));
        }

    }

    /**
     * Получить значение для зума стола
     * @private
     */
    private getZoomValue(): number
    {
        let zoom = 1;
        if (!this.container) {
            return zoom;
        }
        const containerHeight = this.container.offsetHeight - this.containerPadding * 2;

        const values = this.tableItems.map((item) => {
            return item.getItemYPosition() + item.getItemHeight();
        });

        const max =  Math.max.apply(null, values);

        if (max > containerHeight) {
            zoom = containerHeight / max;
        } else {
            zoom = 1;
        }

        return zoom;
    }

    /**
     * Получить выбранные колоды
     */
    public getSelectedDecks(): Array<Deck>
    {
        const decks: Array<Deck> = [];
        const selectedItems = this.getSelectedItems();
        selectedItems.forEach((item) => {
            if (item instanceof Deck) {
                decks.push(item);
            }
        });

        return decks;
    }

    /**
     * Объединить карты на столе в колоду
     */
    public createDeckFromSelectedCards(): void
    {
        if (!this.container) {
            return;
        }

        const x = this.container.offsetWidth - this.containerPadding * 2;
        const y = this.containerPadding * 2;
        const selectedItems = this.getSelectedItems();
        const itemsConfigs: Array<TableItem> = [];
        selectedItems.forEach((item, index) => {
           if (item instanceof Item) {
               itemsConfigs.push(item.getItemConfig() as TableItem);
               if (this.container) {
                   item.setItemPosition({
                       x: x,
                       y: y
                   });
               }

               if (index === selectedItems.length - 1) {
                   setTimeout(() => {
                       this.buildDeck(itemsConfigs, x, y);
                       selectedItems.forEach((item) => item.deleteItem());
                   }, 500);
               }
           }
        });
    }

    /**
     * Перемешать колоду
     */
    public shuffleDeck(): void
    {
        const selectedDecks = this.getSelectedDecks();
    }

    private bindEvents(): void
    {

    }

}