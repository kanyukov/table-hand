/**
 * Апи для работы с рукой
 */
import {CardSetup, HandItem, RenderConfig} from "@/types/types";
import Item from "@/api/Item";
import {Layer} from "konva/types/Layer";
import EventApi from "@/api/EventsApi";
import RenderManager from "@/api/render/RenderManager";
import RenderHandManager from "@/api/render/RenderHandManager";

export default class Hand {
    private handItems: Array<Item> = [];
    private hand: Layer|null = null;
    private readonly cardXOffsetDelta: number = 36;
    private readonly cardWidth: number = 112;
    private readonly cardHeight: number = 154;
    private readonly container: null|HTMLElement = null;
    private readonly tableContainer: null|HTMLElement = null;
    private readonly handName: string = 'hand';
    private readonly tableName: string = 'table';

    /**
     * Constructor
     * @param {Object} config
     */
    constructor(config: RenderConfig)
    {
        this.container = config.handContainer;
        this.tableContainer = config.tableContainer;
        this.createHand(config);
        this.createItems(this.buildItemsConfigs(config.handImages));
        this.bindEvents();
    }

    /**
     * Созать конфиг для итемов руки
     * @param cardsSetup
     * @private
     */
    private buildItemsConfigs(cardsSetup: Array<CardSetup>): Array<HandItem>
    {
        const config: Array<HandItem> = [];
        let x = 0;
        let order = 0;
        let zIndex = cardsSetup.length - 1;

        cardsSetup.forEach((card) => {
            config.push({
                face_src: card.face_src,
                backface_src: card.backface_src,
                id: card.id,
                order: order,
                width: this.cardWidth,
                height: this.cardHeight,
                x: x,
                zIndex: zIndex
            });
            x += this.cardXOffsetDelta;
            order++;
            zIndex--;
        });

        return config;
    }

    /**
     * Получить максимальный X
     * @private
     */
    private getMaxXOffset(): number
    {
        const xValues = this.handItems.map((item) => {
            return item.getItemXPosition();
        });

        return Math.max.apply(null, xValues);
    }

    /**
     * Обновить X карточек
     * @private
     */
    public refreshCardsXPosition(): void {
        let x = 0;
        let zIndex = this.handItems.length - 1;
        this.handItems.forEach((item) => {
            item.setXPosition(x + this.cardWidth/2);
            item.setZIndex(zIndex);
            x += this.cardXOffsetDelta;
            zIndex--;
        });

        RenderHandManager.refreshHand();
    }

    /**
     * Создать итемы руки
     */
    private createItems(items: Array<HandItem>): void
    {
        items.forEach((item) => {
            this.handItems.push(new Item(item, this.container as HTMLElement, this.handName));
        });

        this.refreshCardsXPosition();
    }

    /**
     * Создать слой руки
     */
    private createHand(config: RenderConfig): void
    {
        RenderManager.renderHand(config);
        this.hand = RenderManager.getHandInstance();
    }

    /**
     * Получить выбранные эл-ты
     * @private
     */
    public getSelectedItems(): Array<Item>
    {
        return this.handItems.filter((item) => {
            return item.getItemName() === 'selected';
        });
    }

    /**
     * Получить выбранные эл-ты
     * @private
     */
    public getHoveredItems(): Array<Item>
    {
        return this.handItems.filter((item) => {
            return item.getItemName().indexOf('hovered') !== -1;
        });
    }

    /**
     * Получить эл-ты по ID
     * @private
     */
    private getItemsById(id: string): Array<Item>
    {
        return this.handItems.filter((item) => {
            return item.getItemID() === id;
        });
    }

    /**
     * Удалить выбранные эл-ты из массива эл-тов руки
     * @param items
     * @private
     */
    private deleteItemsFromItemsArray(items: Array<Item>): void
    {
        this.handItems.forEach((item, index) => {
            items.forEach((selectedItem) => {
                const itemID = item.getItemID();
                const selectedItemID = selectedItem.getItemID();

                if (itemID === selectedItemID) {
                    this.handItems.splice(index, 1);
                }
            });
        });
    }

    /**
     * Удалить выбранные итемы руки
     */
    public deleteSelectedItems(itemId: string|null): void
    {
        let selectedItems = this.getSelectedItems();

        if (itemId) {
            selectedItems = this.getItemsById(itemId);
        }

        selectedItems.forEach((item) => {
            item.deleteItem();
        });
        this.deleteItemsFromItemsArray(selectedItems);
        this.refreshCardsXPosition();
        RenderManager.updateStage(this.handItems.length, this.handName);
    }

    /**
     * Удалить hovered итемы руки
     */
    public deleteHoveredItems(): void
    {
        let selectedItems = this.getHoveredItems();

        selectedItems.forEach((item) => {
            item.deleteItem();
        });
        this.deleteItemsFromItemsArray(selectedItems);
        this.refreshCardsXPosition();
        RenderManager.updateStage(this.handItems.length, this.handName);
    }

    /**
     * Перевернуть выбранные итемы руки
     */
    public flipSelectedItems(): void
    {
        this.getSelectedItems().forEach((item) => {
            item.flipItem();
        });
    }

    /**
     * Увеличить выбранные итемы руки
     */
    public zoomSelectedItems(): void
    {
        let images: Array<string> = [];
        this.getSelectedItems().forEach((item) => {
            images.push(item.getItemFaceUrl());
        });

        this.getHoveredItems().forEach((item) => {
            images.push(item.getItemFaceUrl());
        });

        EventApi.triggerEvent(EventApi.ZOOM_ITEMS, images);
    }

    /**
     * Добавить итем в руку
     * @param imgUrl
     */
    public addItem(imgUrl: string): void
    {
        this.handItems.push(new Item({
            face_src: imgUrl,
            backface_src: '/backface.png',
            id: `card_id_${Date.now()}`,
            order: this.handItems.length,
            x: this.getMaxXOffset() - this.cardWidth/2 + this.cardXOffsetDelta,
            width: this.cardWidth,
            height: this.cardHeight,
            zIndex: 0
        }, this.container as HTMLElement, this.handName));
        this.refreshCardsXPosition();
        RenderManager.updateStage(this.handItems.length, this.handName);
    }

    /**
     * Сортировка эл-тов руки по оси Х
     * @private
     */
    private sortItems(): any
    {
        this.handItems.sort(function(a, b) {
            return a.getItemXPosition() - b.getItemXPosition();
        });
        this.refreshCardsXPosition();
    }

    /**
     *
     * @param data
     * @private
     */
    private moveItemToTable(data: any): void
    {
        if (!data || !this.tableContainer) {
            return;
        }

        const item: Item = data.detail;
        item.setContainer(this.tableContainer);
        item.setType(this.tableName);
        item.animateMovingFromHandToTable();
    }

    /**
     * Отменить выбор выбранных эл-тов
     * @private
     */
    private deselectSelectedItems(): void
    {
        const items = this.getSelectedItems();
        items.forEach((item) => {
            item.deselectItem()
        });
    }

    private bindEvents(): void
    {
        EventApi.addEventListener(EventApi.ITEM_DRAGEND_HAND, this.sortItems.bind(this));
        EventApi.addEventListener(EventApi.HAND_TO_TABLE_ANIMATE, this.moveItemToTable.bind(this));
        EventApi.addEventListener(EventApi.DESELECT_HAND_ITEMS, this.deselectSelectedItems.bind(this));
    }

    private unbindEvents(): void
    {
        EventApi.removeEventListener(EventApi.ITEM_DRAGEND_HAND, this.sortItems.bind(this));
    }

}