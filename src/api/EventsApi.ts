/**
 * Апи для работы с событиями
 */
export default class EventApi
{
    public static readonly SELECT_ITEM = 'select_item';
    public static readonly DESELECT_ITEM = 'deselect_item';
    public static readonly DESELECT_HAND_ITEMS = 'deselect_hand_items';
    public static readonly DESELECT_TABLE_ITEMS = 'deselect_table_items';
    public static readonly DELETE_SELECTED_ITEM = 'delete_selected_item';
    public static readonly FLIP_SELECTED_ITEM = 'flip_selected_item';
    public static readonly REFRESH_HAND_ITEM_POSITION = 'refresh_hand_item_position';
    public static readonly REFRESH_TABLE_ITEM_POSITION = 'refresh_table_item_position';
    public static readonly REFRESH_HAND_ITEMS_ALL_POSITIONS = 'refresh_hand_items_all_positions';
    public static readonly ITEM_DRAGEND_HAND = 'item_dragend_hand';
    public static readonly ADD_TABLE_ITEM = 'item_dragend_table';
    public static readonly ZOOM_ITEMS = 'zoom_items';
    public static readonly HAND_TO_TABLE_ANIMATE = 'hand_to_table_animate';

    private static preventKeyEvents: boolean = false;

    public static getPreventKeyEvents(): boolean
    {
        return this.preventKeyEvents;
    }
    public static setPreventKeyEvents(value: boolean): void
    {
        this.preventKeyEvents = value;
    }


    /**
     * Тригер события
     * @param {string} eventName
     * @param data
     */
    public static triggerEvent(eventName: string, data?: any): void
    {
        if (typeof CustomEvent === 'function') {
            let event = new CustomEvent(eventName, {
                detail: data
            });
            document.dispatchEvent(event);
        } else {
            let event = document.createEvent('CustomEvent');
            event.initCustomEvent(eventName, false, false, data);
            document.dispatchEvent(event);
        }
    }

    /**
     * Добавление слушателя событий
     * @param eventName
     * @param handler
     */
    public static addEventListener(eventName: string, handler: EventListenerOrEventListenerObject): void
    {
        document.addEventListener(eventName, handler, false);
    }

    /**
     * Удаление слушателя событий
     * @param eventName
     * @param handler
     */
    public static removeEventListener(eventName: string, handler: EventListenerOrEventListenerObject): void
    {
        document.removeEventListener(eventName, handler, false);
    }
}
