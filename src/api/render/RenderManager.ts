import Konva from "konva";
/**
 * Менеджер рендера конвы
 */
import {
    AnimateItemOnConnectorStageOptions,
    ConnectorStageAddItemOptions,
    ConnectorStageOptions, DeckOptions,
    HandItem,
    RenderConfig,
    TableItem
} from "@/types/types";
import RenderHandManager from "@/api/render/RenderHandManager";
import RenderHandSceneManager from "@/api/render/RenderHandSceneManager";
import {Layer} from "konva/types/Layer";
import RenderHandItemsManager from "@/api/render/RenderHandItemsManager";
import {Group} from "konva/types/Group";
import EventApi from "@/api/EventsApi";
import {Image} from "konva/types/shapes/Image";
import RenderConnectorSceneManager from "@/api/render/RenderConnectorSceneManager";
import RenderTableSceneManager from "@/api/render/RenderTableSceneManager";
import RenderTableManager from "@/api/render/RenderTableManager";
import RenderTableItemsManager from "@/api/render/RenderTableItemsManager";
import Item from "@/api/Item";
import RenderTableDeckManager from "@/api/render/RenderTableDeckManager";

export default class RenderManager {
    public static config: RenderConfig;
    private static readonly handName: string = 'hand';
    private static readonly tableName: string = 'table';
    /**
     * Рендер руки
     */
    public static renderHand(config: RenderConfig)
    {
        this.config = config;
        RenderHandSceneManager.createStage(config);
        RenderHandManager.createHand();
    }

    /**
     * Получить инстанс руки
     */
    public static getHandInstance(): Layer|null
    {
        return RenderHandManager.getHand();
    }

    /**
     * Обновить сцену
     * @param itemsAmount
     */
    public static updateStage(itemsAmount: number, type: string): void
    {
        if (type === this.handName) {
            RenderHandSceneManager.updateStageWidth(itemsAmount);
        }
    }

    /**
     * Рендер карточки руки
     * @param item
     */
    public static renderHandItem(item: HandItem): Group
    {
        return  RenderHandItemsManager.createHandItem(item);
    }

    /**
     * Перевернуть итем
     * @param item
     */
    public static flipItem(item: Group): void
    {
        item.fire(EventApi.FLIP_SELECTED_ITEM);
    }

    /**
     * Выбрать итем
     * @param item
     */
    public static selectItem(item: Group): void
    {
        item.fire(EventApi.SELECT_ITEM);
    }

    /**
     * Выбрать итем
     * @param item
     */
    public static deselectItem(item: Group): void
    {
        item.fire(EventApi.DESELECT_ITEM);
    }

    /**
     * Удалить итем
     * @param item
     */
    public static deleteItem(item: Group): void
    {
        item.fire(EventApi.DELETE_SELECTED_ITEM);
    }

    /**
     * Установить позицию х итема руки
     * @param item
     * @param x
     */
    public static setHandItemXPosition(item: Group, x: number): void
    {
        RenderHandItemsManager.setXPosition(item, x);
    }

    /**
     * Установить позицию х итема стола
     * @param item
     * @param position
     */
    public static setTableItemPosition(item: Group, position: {[key: string]: number}): void
    {
        RenderTableItemsManager.setItemPosition(item, position);
    }

    /**
     * Получить Х итема
     * @param item
     */
    public static getItemXPosition(item: Group): number
    {
        return item.x();
    }

    /**
     * Получить Y итема
     * @param item
     */
    public static getItemYPosition(item: Group): number
    {
        return item.getAttr('y');
    }

    /**
     * Получить имя итема
     * @param item
     */
    public static getItemName(item: Group): string
    {
        return item.name();
    }

    /**
     * Получить ID итема
     * @param item
     */
    public static getItemID(item: Group): string
    {
        return item.id();
    }

    /**
     * Получить ширину итема
     * @param item
     */
    public static getItemWidth(item: Group): number
    {
        return item.width();
    }

    /**
     * Получить ширину итема
     * @param item
     */
    public static getItemHeight(item: Group): number
    {
        return item.height();
    }

    /**
     * Установить ширину итема
     * @param item
     * @param width
     */
    public static setItemWidth(item: Group, width: number): void
    {
        item.width(width);
    }

    /**
     * Установить ширину итема
     * @param item
     * @param height
     */
    public static setItemHeight(item: Group, height: number): void
    {
        item.height(height);
    }

    /**
     * Установить z-index итема
     * @param item
     * @param index
     */
    public static setItemZIndex(item: Group, index: number): void
    {
        item.zIndex(index);
    }

    /**
     * Получить лицевую сторону карты
     * @param item
     */
    public static getItemFace(item: Group): Image|null
    {
        const faceSideArray = item.getChildren((node) => {
            return node.name() === 'face';
        }).toArray();
        if (typeof faceSideArray[0] === 'undefined') {
            return null;
        }
        return faceSideArray[0] as Image;
    }

    /**
     * Получить урл лицевой стороны карты
     * @param item
     */
    public static getItemFaceUrl(item: Group): string
    {
        const faceSideArray = item.getChildren((node) => {
            return node.name() === 'face';
        }).toArray();

        if (typeof faceSideArray[0] === 'undefined') {
            return '';
        }
        console.log()
        const faceSideImage = (faceSideArray[0] as Image).image();
        const faceUrl = (faceSideImage as HTMLImageElement).src;
        return faceUrl;
    }

    /**
     * Получить урл рубашки карты
     * @param item
     */
    public static getItemBackfaceUrl(item: Group): string
    {
        const backfaceSideArray = item.getChildren((node) => {
            return node.name() === 'backface';
        }).toArray();

        if (typeof backfaceSideArray[0] === 'undefined') {
            return '';
        }
        const backfaceSideImage = (backfaceSideArray[0] as Image).image();
        const backfaceUrl = (backfaceSideImage as HTMLImageElement).src;
        return backfaceUrl;
    }

    /**
     * деактивировать карту руки
     * @param item
     */
    public static disableHandItem(item: Group): void
    {
        item.opacity(0.5);
        item.draggable(false);
        RenderHandManager.refreshHand();
    }

    /**
     * активировать карту
     * @param item
     */
    public static enableHandItem(item: Group): void
    {
        item.opacity(1);
        item.draggable(true);
        RenderHandManager.refreshHand();
    }

    /**
     * Создать связывающий стол и руку слой
     * @param options
     */
    public static createConnectorStage(options: ConnectorStageOptions): void
    {
        const item = options.item;
        const itemGroup = item.getItem();
        RenderConnectorSceneManager.createStage(this.config);
        const clonedItem = item.cloneItem();
        clonedItem.getItem()?.position({
            x: options.newX,
            y: options.newY
        })

        if (itemGroup) {
            this.disableHandItem(itemGroup);
        }

        const addItemConfig: ConnectorStageAddItemOptions = {
            item: clonedItem,
            oldItem: item
        }

        RenderConnectorSceneManager.addItem(addItemConfig);
    }

    /**
     * Анимировать перемещение эл-та по связывающему слою
     * @param options
     */
    public static animateItemMovingOnConnectorStage(options: AnimateItemOnConnectorStageOptions): void
    {
        const item = options.item;
        const itemGroup = item.getItem();
        RenderConnectorSceneManager.createStage(this.config);
        const clonedItem = item.cloneItem();
        item.deleteItem();
        clonedItem.getItem()?.position({
            x: options.newX,
            y: options.newY
        })

        const addItemConfig: ConnectorStageAddItemOptions = {
            item: clonedItem,
            oldItem: item
        }

        RenderConnectorSceneManager.addItem(addItemConfig);
        RenderConnectorSceneManager.animateMoving({x: options.newX1, y: options.newY1})

    }

    /**
     * Рендер стола
     */
    public static renderTable(config: RenderConfig)
    {
        this.config = config;
        RenderTableSceneManager.createStage(config);
        RenderTableManager.createTable();
    }

    /**
     * Получить инстанс стола
     */
    public static getTableInstance(): Layer|null
    {
        return RenderTableManager.getTable();
    }

    /**
     * Рендер карточки стола
     * @param item
     */
    public static renderTableItem(item: TableItem): Group
    {
        return  RenderTableItemsManager.createTableItem(item);
    }

    /**
     * Рендер карточки стола
     */
    public static renderTableDeck(items: Array<Item>, options: DeckOptions): Group
    {
        return  RenderTableDeckManager.createTableDeck(items, options);
    }
}