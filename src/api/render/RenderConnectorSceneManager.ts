import Konva from "konva";
/**
 * Менеджер рендера конвы
 */
import {Stage} from "konva/types/Stage";
import {ConnectorStageAddItemOptions, RenderConfig} from "@/types/types";
import {Layer} from "konva/types/Layer";
import {Group} from "konva/types/Group";
import EventApi from "@/api/EventsApi";
import RenderManager from "@/api/render/RenderManager";
import {Vector2d} from "konva/types/types";
import RenderHandManager from "@/api/render/RenderHandManager";
import Item from "@/api/Item";

export default class RenderConnectorSceneManager {
    public static stage: Stage|null = null;
    public static layer: Layer|null = null;
    public static currentItem: Item|null = null;
    public static oldItem: Item|null = null;
    public static handContainer: HTMLElement|null = null;
    public static tableContainer: HTMLElement|null = null;

    /**
     * Создать сцену
     * @param config
     * @private
     */
    public static createStage(config: RenderConfig): void
    {
        this.handContainer = config.handContainer;
        this.tableContainer = config.tableContainer;
        this.layer = new Konva.Layer();
        this.stage = new Konva.Stage({
            container: config.connectorContainer.id,
            width: window.innerWidth,
            height: window.innerHeight
        });
        this.stage.zIndex(3);
        this.stage.add(this.layer);

        this.stage.on('dragmove', (e) => {
            const cursorPosition = this.stage?.getPointerPosition();

            if (!this.stage) {
                return;
            }
            this.stage.container().style.cursor = 'no-drop';
            if (this.hasIntersectionWithHand(cursorPosition as Vector2d) || this.hasIntersectionWithTable(cursorPosition as Vector2d)) {
                this.stage.container().style.cursor = 'pointer';
            }

        });

        this.stage.on('dragend', (e) => {
            if (!this.currentItem || !this.oldItem || !this.handContainer || !this.handContainer) {
                return;
            }

            const currentGroup = this.currentItem.getItem();
            const oldGroup = this.oldItem.getItem();

            if (!currentGroup || !oldGroup) {
                return;
            }

            const cursorPosition = this.stage?.getPointerPosition();
            if (this.hasIntersectionWithHand(cursorPosition as Vector2d)) {
                const newX = currentGroup.x() - this.handContainer.getBoundingClientRect().x;
                RenderManager.setHandItemXPosition(oldGroup, newX);
                RenderManager.enableHandItem(oldGroup);
                EventApi.triggerEvent(EventApi.ITEM_DRAGEND_HAND);
            } else if (this.hasIntersectionWithTable(cursorPosition as Vector2d)) {
                RenderManager.deleteItem(oldGroup);
                EventApi.triggerEvent(EventApi.ADD_TABLE_ITEM, [this.currentItem]);
            } else {
                if (!this.oldItem) {
                    return;
                }
                RenderManager.enableHandItem(oldGroup);
            }

            this.destroyStage();

        });

        this.stage.on('mousemove', () => {

        });
    }

    /**
     * Курсор в зоне руки
     * @param cursorPosition
     */
    public static hasIntersectionWithHand(cursorPosition: Vector2d): boolean
    {
        if (!this.handContainer) {
            return false;
        }
        const handContainerX = this.handContainer.getBoundingClientRect().x;
        const handContainerY = this.handContainer.getBoundingClientRect().y;
        const handContainerMaxX = this.handContainer.offsetWidth + handContainerX;
        const handContainerMaxY = this.handContainer.offsetHeight + handContainerY;

        if ((cursorPosition.x >= handContainerX && cursorPosition.x <= handContainerMaxX)
            && (cursorPosition.y >= handContainerY && cursorPosition.y <= handContainerMaxY)) {
            return true;
        }

        return false;
    }

    /**
     * Курсор в зоне стола
     * @param cursorPosition
     */
    public static hasIntersectionWithTable(cursorPosition: Vector2d): boolean
    {
        if (!this.tableContainer) {
            return false;
        }
        const tableContainerX = this.tableContainer.getBoundingClientRect().x;
        const tableContainerY = this.tableContainer.getBoundingClientRect().y;
        const tableContainerMaxX = this.tableContainer.offsetWidth + tableContainerX;
        const tableContainerMaxY = this.tableContainer.offsetHeight + tableContainerY;

        if ((cursorPosition.x >= tableContainerX && cursorPosition.x <= tableContainerMaxX)
            && (cursorPosition.y >= tableContainerY && cursorPosition.y <= tableContainerMaxY)) {
            return true;
        }

        return false;
    }

    /**
     * Получить сцену
     */
    public static getStage(): Stage|null
    {
        return this.stage;
    }

    /**
     * Удалить сцену
     */
    public static destroyStage(): void
    {
        if (!this.stage) {
            return;
        }
        this.stage.destroy();
    }

    /**
     * Добавить карту на сцену
     * @param options
     */
    public static addItem(options: ConnectorStageAddItemOptions): void
    {
        if (!this.layer || !this.stage) {
            return;
        }
        const item = options.item;
        this.oldItem = options.oldItem;
        this.currentItem = item;
        const currentGroup = item.getItem();

        if (!currentGroup) {
            return;
        }

        currentGroup.off('dragstart');
        currentGroup.off('dragend');
        this.layer.add(currentGroup);
        currentGroup.zIndex(10);
        currentGroup.startDrag();
        currentGroup.on('mouseover', () => {
            if (!this.stage) {
                return;
            }
            this.stage.container().style.cursor = 'pointer';
        })
        currentGroup.on('mouseleave', () => {
            if (!this.stage) {
                return;
            }
            this.stage.container().style.cursor = 'default';
        })
        this.layer.draw();
    }

    public static animateMoving(options: {[key: string]: number}): void
    {
        const item = this.currentItem?.getItem();
        if (item && this.stage) {
            item.startDrag();
            item.to({
                x: options.x,
                y: options.y
            })

        }

    }
}