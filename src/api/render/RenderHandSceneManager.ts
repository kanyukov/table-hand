import Konva from "konva";
/**
 * Менеджер рендера конвы
 */
import {Stage} from "konva/types/Stage";
import {RenderConfig} from "@/types/types";
import EventApi from "@/api/EventsApi";

export default class RenderHandSceneManager {
    public static stage: Stage|null = null;

    /**
     * Создать сцену
     * @param config
     * @private
     */
    public static createStage(config: RenderConfig): void
    {
        this.stage = new Konva.Stage({
            container: config.handContainer.id,
            width: this.calcStageWidth(config.handImages.length),
            height: config.handContainer.offsetHeight
        });
        this.bindEvents();
    }

    /**
     * Получить сцену
     */
    public static getStage(): Stage|null
    {
        return this.stage;
    }

    /**
     * Посчитать ширину сцены
     * @param itemsAmount
     */
    public static calcStageWidth(itemsAmount : number): number
    {
        const cardWidth = 112;
        const delta = 36;
        return delta * (itemsAmount - 1) + cardWidth + cardWidth/3;
    }

    /**
     * Обновить ширину сцены
     * @param itemsAmount
     */
    public static updateStageWidth(itemsAmount: number): void
    {
        if (!this.stage) {
            return;
        }
        this.stage.width(this.calcStageWidth(itemsAmount));
        this.stage.draw();
    }

    private static bindEvents(): void
    {
        if (!this.stage) {
            return;
        }

        this.stage.on('click', (e) => {
            if ((e.target as any).bufferCanvas) {
                EventApi.triggerEvent(EventApi.DESELECT_HAND_ITEMS);
                EventApi.triggerEvent(EventApi.DESELECT_TABLE_ITEMS);
            }
        });
    }
}