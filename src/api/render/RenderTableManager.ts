import Konva from "konva";
/**
 * Менеджер рендера руки
 */
import {Layer} from "konva/types/Layer";
import RenderTableSceneManager from "@/api/render/RenderTableSceneManager";
import {Group} from "konva/types/Group";

export default class RenderTableManager {
    public static table: Layer|null = null;

    /**
     * Создать стол
     */
    public static createTable(): void
    {
        this.table = new Konva.Layer();
        const stage = RenderTableSceneManager.getStage();

        if (!stage) {
            return;
        }

        stage.add(this.table);
    }

    /**
     * Получить руку
     */
    public static getTable(): Layer|null
    {
        return this.table;
    }

    /**
     * Перерисовать стол
     */
    public static refreshTable(): void
    {
        if (!this.table) {
            return;
        }
        this.table.draw();
    }

    public static addItem(item: Group): void
    {
        if (!this.table) {
            return;
        }

        this.table.add(item);
    }
}