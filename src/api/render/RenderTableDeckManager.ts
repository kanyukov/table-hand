/**
 * Менеджер рендера итемов руки
 */
import {DeckOptions, TableItem} from "@/types/types";
import Image = Konva.Image;
import {Layer} from "konva/types/Layer";
import Konva from "konva";
import {Group} from "konva/types/Group";
import EventApi from "@/api/EventsApi";
import RenderTableSceneManager from "@/api/render/RenderTableSceneManager";
import {Stage} from "konva/types/Stage";
import RenderTableManager from "@/api/render/RenderTableManager";
import Item from "@/api/Item";

export default class RenderTableDeckManager {
    public static selectedName: string = 'selected';
    public static notSelectedName: string = 'not-selected';

    /**
     * Создать стопку карт на столе
     */
    public static createTableDeck(items: Array<Item>, options: DeckOptions): Group
    {
        const table = RenderTableManager.getTable();
        const deck = new Konva.Group({
            x: options.x + options.width/2,
            y: options.y,
            width: options.width,
            height: options.height,
            id: `deck_id_${Date.now()}`,
            name: this.notSelectedName,
            draggable: true,
        });

        items.forEach((item, index) => {
            const group = item.getItem();
            if (group) {
                group.x(0);
                group.y(0);
                group.draggable(false);
                if (index !== items.length - 1 && index % 2 === 0) {
                    group.offsetX(options.width / 2 + 8);
                    // group.offsetY(5);
                    group.rotate(-5);
                    group.off('mouseover mouseleave click');
                }
                if (index !== items.length - 1 && index % 2 !== 0) {
                    group.offsetX(options.width / 2 - 1);
                    // group.offsetY(-5);
                    group.rotate(2);
                    group.off('mouseover mouseleave click');
                }
                deck.add(group);
            }
        });

        deck.offsetX(options.width / 2);

        if (table) {
            this.bindEvents(deck);
            table.add(deck);
            RenderTableManager.refreshTable();
        }

        return deck;
    }

    /**
     * Выбрать/отменить выбор карточки
     * @param deck
     */
    public static toggleSelectItem(deck: Group): void
    {
        const isSelected = deck.name() === this.selectedName;


        if (!isSelected) {
            deck.name(this.selectedName);
            deck.find('Group')
                .toArray()
                .forEach((item) => {
                    item.to({
                        strokeWidth: 3
                    })
                });
            RenderTableManager.refreshTable();
        } else {
            deck.name(this.notSelectedName);
            deck.find('Group')
                .toArray()
                .forEach((item) => {
                    item.to({
                        strokeWidth: 0
                    })
                });
            RenderTableManager.refreshTable();
        }
    }

    /**
     * События ховера
     * @param deck
     */
    public static bindHoverEvents(deck: Group): void
    {
        let startZIndex = 0;

        deck.on('mouseover',  () => {
            startZIndex = deck.zIndex();
            deck.zIndex(100);
            if (RenderTableSceneManager.getStage()) {
                (RenderTableSceneManager.getStage() as Stage).container().style.cursor = 'pointer';
            }
            RenderTableManager.refreshTable();
        });

        deck.on('mouseleave',  () => {
            deck.zIndex(startZIndex);
            if (RenderTableSceneManager.getStage()) {
                (RenderTableSceneManager.getStage() as Stage).container().style.cursor = 'default';
            }
            RenderTableManager.refreshTable();
        });
    }

    /**
     * События клика
     * @param deck
     */
    public static bindClickEvents(deck: Group): void
    {
        deck.on('click',  () => {
            this.toggleSelectItem(deck);
        });
    }

    /**
     * Инициализация событий
     * @param deck
     */
    public static bindEvents(deck: Group): void
    {
        this.bindHoverEvents(deck);
        this.bindClickEvents(deck);
        this.deleteItem(deck);
        this.resetX(deck);
    }

    /**
     * Удалить итем
     * @param deck
     */
    public static deleteItem(deck: Group): void
    {
        deck.on(EventApi.DELETE_SELECTED_ITEM, () => {

            deck.destroy();
            RenderTableManager.refreshTable();
        })
    }

    /**
     * Обновить позицию карты по оси Х
     * @param deck
     */
    public static resetX(deck: Group): void
    {
        deck.on(EventApi.REFRESH_TABLE_ITEM_POSITION, (e) => {
            deck.to({
                x: (e as any).x,
                y: (e as any).y
            })
        })
    }

    public static setItemPosition(deck: Group, position: {[key: string]: number}): void
    {
        deck.to({
            x: position.x,
            y: position.y
        })
        RenderTableManager.refreshTable();
    }
}