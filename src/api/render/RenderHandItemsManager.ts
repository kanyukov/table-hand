/**
 * Менеджер рендера итемов руки
 */
import {HandItem} from "@/types/types";
import Image = Konva.Image;
import {Layer} from "konva/types/Layer";
import Konva from "konva";
import RenderHandManager from "@/api/render/RenderHandManager";
import {Group} from "konva/types/Group";
import EventApi from "@/api/EventsApi";
import RenderHandSceneManager from "@/api/render/RenderHandSceneManager";
import {Stage} from "konva/types/Stage";

export default class RenderHandItemsManager {
    public static selectedName: string = 'selected';
    public static notSelectedName: string = 'not-selected';

    /**
     *
     * @param item
     */
    public static createHandItem(item: HandItem): Group
    {
        const hand = RenderHandManager.getHand();
        const handItem = new Konva.Group({
            x: item.x + item.width/2,
            y: 22,
            width: item.width,
            height: item.height,
            id: item.id,
            name: this.notSelectedName,
            draggable: true,
        });

        Konva.Image.fromURL(item.backface_src, (card_backface: Image) => {
            card_backface.setAttrs({
                x: item.width,
                y: 0,
                width: item.width,
                height: item.height,
                stroke: 'red',
                strokeWidth: 0,
                scaleX: -1,
                opacity: 0,
                name: 'backface'
            });
            handItem.add(card_backface);
            handItem.draw();
        });

        Konva.Image.fromURL(item.face_src, (card_face: Image) => {
            card_face.setAttrs({
                x: 0,
                y: 0,
                width: item.width,
                height: item.height,
                stroke: 'red',
                strokeWidth: 0,
                name: 'face'
            });
            handItem.add(card_face);
            handItem.draw();
        });

        handItem.offsetX(item.width / 2);

        if (hand) {
            this.bindEvents(handItem, hand);
            hand.add(handItem);
            RenderHandManager.refreshHand();
        }

        return handItem;
    }

    /**
     * Выбрать карточку
     * @param card
     * @param hand
     */
    public static clickItem(card: Group, hand: Layer): void
    {
        card.on('click', (e) => {
            const isSelected = card.name().indexOf(this.selectedName) === 0;

            const selectItem = () => {
                card.name(this.selectedName);
                card.find('Image')
                    .toArray()
                    .forEach((image) => {
                        image.to({
                            strokeWidth: 3
                        })
                    });
            };

            const deselectItem = () => {
                card.name(this.notSelectedName);
                card.find('Image')
                    .toArray()
                    .forEach((image) => {
                        image.to({
                            strokeWidth: 0
                        })
                    });
            };

            if (isSelected) {
                deselectItem();
            } else {
                if (!e.evt.shiftKey) {
                    EventApi.triggerEvent(EventApi.DESELECT_HAND_ITEMS);
                }
                selectItem();
            }

            RenderHandManager.refreshHand();
        })
    }

    /**
     * Отменить выбор карточки
     * @param card
     * @param hand
     */
    public static deselectItem(card: Group, hand: Layer): void
    {
        card.on(EventApi.DESELECT_ITEM, () => {
            card.name(this.notSelectedName);
            card.find('Image')
                .toArray()
                .forEach((image) => {
                    image.to({
                        strokeWidth: 0
                    })
                });
            RenderHandManager.refreshHand();
        });
    }

    /**
     * События ховера
     * @param card
     * @param hand
     */
    public static bindHoverEvents(card: Group, hand: Layer): void
    {
        let startZIndex = 0;

        card.on('mouseover',  () => {
            startZIndex = card.zIndex();
            card.zIndex(100);
            card.name(`${card.name()} hovered`);

            if (RenderHandSceneManager.getStage()) {
                (RenderHandSceneManager.getStage() as Stage).container().style.cursor = 'pointer';
            }
            RenderHandManager.refreshHand();
        });

        card.on('mouseleave',  () => {
            let isSelected = card.name().indexOf(this.selectedName) === 0;
            card.zIndex(startZIndex);
            if (isSelected) {
                card.name(this.selectedName);
            } else {
                card.name(this.notSelectedName);
            }

            if (RenderHandSceneManager.getStage()) {
                (RenderHandSceneManager.getStage() as Stage).container().style.cursor = 'default';
            }
            RenderHandManager.refreshHand();
        });
    }

    /**
     * Инициализация событий
     * @param card
     * @param hand
     */
    public static bindEvents(card: Group, hand: Layer): void
    {
        this.bindHoverEvents(card, hand);
        this.clickItem(card, hand);
        this.deselectItem(card, hand);
        this.flipItem(card, hand);
        this.deleteItem(card, hand);
        this.resetX(card, hand);
    }

    /**
     * Переворот карты рубашкой вверх и обратно
     * @param card
     * @param hand
     */
    public static flipItem(card: Group, hand: Layer): void
    {
        card.on(EventApi.FLIP_SELECTED_ITEM, () => {
            const currentScale = card.getAttr('scaleX');
            const cardWidth = card.getAttr('width');

            if (card.name() !== this.selectedName) {
                return;
            }

            card.to({
                scaleX: currentScale === 1 ? -1 : 1
            });
            card.offsetX(cardWidth / 2);

            card.find('Image')
                .toArray()
                .forEach((image) => {
                    if (currentScale === 1) {
                        if (image.getAttr('name') === 'face') {
                            image.zIndex(0);
                            image.to({
                                opacity: 0
                            });
                        } else {
                            image.zIndex(1);
                            image.to({
                                opacity: 1
                            });
                        }
                    } else {
                        if (image.getAttr('name') === 'face') {
                            image.zIndex(1);
                            image.to({
                                opacity: 1
                            });
                        } else {
                            image.zIndex(0);
                            image.to({
                                opacity: 0
                            });
                        }
                    }
                })
        })
    }

    /**
     * Удалить итем
     * @param card
     * @param hand
     */
    public static deleteItem(card: Group, hand: Layer): void
    {
        card.on(EventApi.DELETE_SELECTED_ITEM, () => {

            card.destroy();
            RenderHandManager.refreshHand();
        })
    }

    /**
     * Обновить позицию карты по оси Х
     * @param card
     * @param hand
     */
    public static resetX(card: Group, hand: Layer): void
    {
        card.on(EventApi.REFRESH_HAND_ITEM_POSITION, (e) => {
            card.to({
                x: (e as any).x,
                y: 22
            })
        })
    }

    public static setXPosition(card: Group, x: number): void
    {
        card.x(x);
        card.fire(EventApi.REFRESH_HAND_ITEM_POSITION, {x: x});
        RenderHandManager.refreshHand();
    }
}