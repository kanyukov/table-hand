import Konva from "konva";
/**
 * Менеджер рендера конвы
 */
import {Stage} from "konva/types/Stage";
import {RenderConfig} from "@/types/types";

export default class RenderTableSceneManager {
    public static stage: Stage;
    private static container: HTMLElement|null = null;

    /**
     * Создать сцену
     * @param config
     * @private
     */
    public static createStage(config: RenderConfig): void
    {
        this.container = config.tableContainer;
        this.stage = new Konva.Stage({
            container: this.container.id,
            width: config.tableContainer.offsetWidth,
            height: config.tableContainer.offsetHeight,
            draggable: true
        });
        this.bindEvents();
    }

    /**
     * Получить сцену
     */
    public static getStage(): Stage|null
    {
        return this.stage;
    }

    /**
     * Зум стола по центру
     * @param scale
     */
    public static zoomTableCenter(scale: number): void
    {
        if (!this.container) {
            return;
        }
        this.stage.scale({ x: scale, y: scale });

        // const newPos = {
        //     x: (this.container.offsetWidth * scale),
        //     y: (this.container.offsetHeight * scale),
        // };
        // this.stage.position(newPos);
        this.stage.batchDraw();
    }

    private static bindEvents(): void
    {
        if (this.stage) {
            const scaleBy = 1.03;
            this.stage.on('wheel', (e) => {
                e.evt.preventDefault();
                const oldScale = this.stage.scaleX();
                const pointer = this.stage.getPointerPosition();

                if (!pointer) {
                    return;
                }
                const mousePointTo = {
                    x: (pointer.x - this.stage.x()) / oldScale,
                    y: (pointer.y - this.stage.y()) / oldScale,
                };

                const newScale =
                    e.evt.deltaY > 0 ? oldScale * scaleBy : oldScale / scaleBy;

                this.stage.scale({ x: newScale, y: newScale });

                const newPos = {
                    x: pointer.x - mousePointTo.x * newScale,
                    y: pointer.y - mousePointTo.y * newScale,
                };
                this.stage.position(newPos);
                this.stage.batchDraw();
            });
        }

    }
}