/**
 * Менеджер рендера итемов руки
 */
import {TableItem} from "@/types/types";
import Image = Konva.Image;
import {Layer} from "konva/types/Layer";
import Konva from "konva";
import {Group} from "konva/types/Group";
import EventApi from "@/api/EventsApi";
import RenderTableSceneManager from "@/api/render/RenderTableSceneManager";
import {Stage} from "konva/types/Stage";
import RenderTableManager from "@/api/render/RenderTableManager";

export default class RenderTableItemsManager {
    public static selectedName: string = 'selected';
    public static notSelectedName: string = 'not-selected';

    /**
     *
     * @param item
     */
    public static createTableItem(item: TableItem): Group
    {
        const table = RenderTableManager.getTable();
        const tableItem = new Konva.Group({
            x: item.x + item.width/2,
            y: item.y,
            width: item.width,
            height: item.height,
            id: item.id,
            name: this.notSelectedName,
            draggable: true,
        });

        Konva.Image.fromURL(item.backface_src, (card_backface: Image) => {
            card_backface.setAttrs({
                x: item.width,
                y: 0,
                width: item.width,
                height: item.height,
                stroke: 'red',
                strokeWidth: 0,
                scaleX: -1,
                opacity: 0,
                name: 'backface'
            });
            tableItem.add(card_backface);
            tableItem.draw();
        });

        Konva.Image.fromURL(item.face_src, (card_face: Image) => {
            card_face.setAttrs({
                x: 0,
                y: 0,
                width: item.width,
                height: item.height,
                stroke: 'red',
                strokeWidth: 0,
                name: 'face'
            });
            tableItem.add(card_face);
            tableItem.draw();
        });

        tableItem.offsetX(item.width / 2);

        if (table) {
            this.bindEvents(tableItem, table);
            table.add(tableItem);
            RenderTableManager.refreshTable();
        }

        return tableItem;
    }

    /**
     * Выбрать/отменить выбор карточки
     * @param card
     * @param hand
     */
    public static toggleSelectItem(card: Group, hand: Layer): void
    {
        const isSelected = card.name() === this.selectedName;


        if (!isSelected) {
            card.name(this.selectedName);
            card.find('Image')
                .toArray()
                .forEach((image) => {
                    image.to({
                        strokeWidth: 3
                    })
                });
            RenderTableManager.refreshTable();
        } else {
            card.to({name: this.notSelectedName});
            card.find('Image')
                .toArray()
                .forEach((image) => {
                    image.to({
                        strokeWidth: 0
                    })
                });
            RenderTableManager.refreshTable();
        }
    }

    /**
     * События ховера
     * @param card
     * @param hand
     */
    public static bindHoverEvents(card: Group, hand: Layer): void
    {
        let startZIndex = 0;

        card.on('mouseover',  () => {
            startZIndex = card.zIndex();
            card.zIndex(100);
            if (RenderTableSceneManager.getStage()) {
                (RenderTableSceneManager.getStage() as Stage).container().style.cursor = 'pointer';
            }
            RenderTableManager.refreshTable();
        });

        card.on('mouseleave',  () => {
            card.zIndex(startZIndex);
            if (RenderTableSceneManager.getStage()) {
                (RenderTableSceneManager.getStage() as Stage).container().style.cursor = 'default';
            }
            RenderTableManager.refreshTable();
        });
    }

    /**
     * События клика
     * @param card
     * @param hand
     */
    public static bindClickEvents(card: Group, hand: Layer): void
    {
        card.on('click',  () => {
            this.toggleSelectItem(card, hand);
        });
    }

    /**
     * Инициализация событий
     * @param card
     * @param hand
     */
    public static bindEvents(card: Group, hand: Layer): void
    {
        this.bindHoverEvents(card, hand);
        this.bindClickEvents(card, hand);
        this.flipItem(card, hand);
        this.deleteItem(card, hand);
        this.resetX(card, hand);
    }

    /**
     * Переворот карты рубашкой вверх и обратно
     * @param card
     * @param hand
     */
    public static flipItem(card: Group, hand: Layer): void
    {
        card.on(EventApi.FLIP_SELECTED_ITEM, () => {
            const currentScale = card.getAttr('scaleX');
            const cardWidth = card.getAttr('width');

            if (card.name() !== this.selectedName) {
                return;
            }

            card.to({
                scaleX: currentScale === 1 ? -1 : 1
            });
            card.offsetX(cardWidth / 2);

            card.find('Image')
                .toArray()
                .forEach((image) => {
                    if (currentScale === 1) {
                        if (image.getAttr('name') === 'face') {
                            image.zIndex(0);
                            image.to({
                                opacity: 0
                            });
                        } else {
                            image.zIndex(1);
                            image.to({
                                opacity: 1
                            });
                        }
                    } else {
                        if (image.getAttr('name') === 'face') {
                            image.zIndex(1);
                            image.to({
                                opacity: 1
                            });
                        } else {
                            image.zIndex(0);
                            image.to({
                                opacity: 0
                            });
                        }
                    }
                })
        })
    }

    /**
     * Удалить итем
     * @param card
     * @param hand
     */
    public static deleteItem(card: Group, hand: Layer): void
    {
        card.on(EventApi.DELETE_SELECTED_ITEM, () => {

            card.destroy();
            RenderTableManager.refreshTable();
        })
    }

    /**
     * Обновить позицию карты по оси Х
     * @param card
     * @param hand
     */
    public static resetX(card: Group, hand: Layer): void
    {
        card.on(EventApi.REFRESH_TABLE_ITEM_POSITION, (e) => {
            card.to({
                x: (e as any).x,
                y: (e as any).y
            })
        })
    }

    public static setItemPosition(card: Group, position: {[key: string]: number}): void
    {
        card.to({
            x: position.x,
            y: position.y
        })
        RenderTableManager.refreshTable();
    }
}