import Konva from "konva";
/**
 * Менеджер рендера руки
 */
import {Layer} from "konva/types/Layer";
import RenderHandSceneManager from "@/api/render/RenderHandSceneManager";

export default class RenderHandManager {
    public static hand: Layer|null = null;

    /**
     * Создать руку
     */
    public static createHand(): void
    {
        this.hand = new Konva.Layer();
        const stage = RenderHandSceneManager.getStage();

        if (!stage) {
            return;
        }

        stage.add(this.hand);
    }

    /**
     * Получить руку
     */
    public static getHand(): Layer|null
    {
        return this.hand;
    }

    /**
     * Перерисовать руку
     */
    public static refreshHand(): void
    {
        if (!this.hand) {
            return;
        }
        this.hand.draw();
    }
}